## Pantahub Running Made easiest

1. clone this project: ```git clone --recurse-submodules https://gitlab.com/pantacor/pantahub-devbench```
2. run: ```docker-compose up -d```
3. use pantahub (remember storage will be reset on every restart)
4. to stop pantahub services use: ```docker-compose down```

## Pantahub Development Made easiest

1. clone this project: ```git clone --recurse-submodules https://gitlab.com/pantacor/pantahub-devbench```
2. find and edit your source in modules/ directory
3. build projects for which you have edited source: ```docker-compose build SVCNAME```
4. when happy commit and submit for merging the source changes
5. to stop pantahub services use: ```docker-compose down```

## Use your local Pantahub

Pantahub: will listen on port ```:12365``` serving HTTP.

Pantahub PVR: will listen on ```:12367```

Pantafleet: will listen on ```:12368```

Example: to retrieve auth token for pantahub base a user can do:

```
http localhost:12365/auth/login username=user1 password=user1
```

